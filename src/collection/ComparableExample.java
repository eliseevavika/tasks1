package collection;

import java.util.Comparator;
import java.util.TreeSet;

/**
 * Created by Виктория on 10.11.2016.
 */
public class ComparableExample  {
    public  static void main(String[] args) {
        TreeSet<Car> car=new TreeSet<>();
        car.add(new Car(2000000, "Jeep", 4));
        car.add(new Car(1091000, "Infiniti", 5));
        car.add(new Car(860000, "BMW", 4));
        car.add(new Car(900000,"Audi", 3));
        System.out.println(car);
    }
    static class Car implements Comparable<Car>, Comparator<Car> {
        int rubles;
        String name;
        int evaluation;

        @Override
        public String toString() {
            return name+"$"+rubles;
        }

        public Car(int rubles,String name, int evaluation  ) {
            this.rubles=rubles;
            this.name = name;
            this.evaluation=evaluation;
        }


        @Override
        public int compareTo( Car o) {
            int diff=o.rubles-rubles;
            if(diff==0){
                return 0;
            }
            if(diff >0){
                return 1;
            }
            else{
                return -1;
            }
        }

        @Override
        public int compare(Car o1, Car o2) {
            return o1.getName().compareTo(o2.getName());
        }

        public String getName() {
            return name;
        }
    }
}

