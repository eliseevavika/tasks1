import java.util.Scanner;

public class ScannerUseDelimenetExample {
    public static void main(String[] args) {


        Scanner scan = new Scanner("Anna Mills/Female/18");
        scan.useDelimiter("/");

        System.out.println("The delimiter use is "+scan.delimiter());
        while(scan.hasNext()){
            System.out.println(scan.next());
        }

        scan.close();

    }

}

