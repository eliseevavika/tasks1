import java.io.*;

/**
 * Created by Виктория on 06.11.2016.
 */
public class ReaderExample {
    public static void main(String[] args) throws IOException {

        Reader reader = new FileReader("D:\\ExampleFile.txt");

        int data1 = reader.read();


        while (data1 != -1) {
            char dataChar = (char) data1;
            data1 = reader.read();
            System.out.print(dataChar);

        }
    }
}