import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class BufferedReaderExample{
    public static void main(String[] args) {

    try(BufferedReader br = new BufferedReader (new FileReader("D:\\ExampleFile.txt")))
    {
        // чтение посимвольно
        int c;
        while((c=br.read())!=-1){

            System.out.print((char)c);
        }
    }
    catch(IOException ex){

        System.out.println(ex.getMessage());
    }
}
}

