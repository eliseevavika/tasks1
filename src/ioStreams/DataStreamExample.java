package ioStreams;

import java.io.*;
 class DataInputOutputStreamExample {
//А зачем вам смотреть в редакторе? Вы же пользователь класса. Запишите через DataOutputStream и прочитайте через DataInputStream и будет вам счастье.
     public static void main(String args[]) throws IOException {
         DataOutputStream dataOutputStream = new DataOutputStream(new FileOutputStream("D:\\ExampleFile.txt"));
     dataOutputStream.writeUTF("String1");
         dataOutputStream.writeUTF("String2");
         dataOutputStream.writeLong(444322222);
         dataOutputStream.writeDouble(12.3);
         dataOutputStream.writeUTF("String3");
         dataOutputStream.close();

         try (DataInputStream dataInputStream = new DataInputStream(new FileInputStream("D:\\ExampleFile.txt"))) {

             dataInputStream.readUTF();
             dataInputStream.readUTF();
             dataInputStream.readLong();
             dataInputStream.readDouble();
             dataInputStream.readUTF();

             System.out.println(dataInputStream.readUTF());
             System.out.println(dataInputStream.readUTF());
             System.out.println(dataInputStream.readLong());
             System.out.println(dataInputStream.readDouble());
             System.out.println(dataInputStream.readUTF());
         } catch (IOException ex) {
             System.out.println("error");
             System.out.println(ex.getMessage());
         }
     }

 }