

import java.io.*;

 class WhiteDemo {

    public static void main(String[] args) {

        String pArray[] = { "2007 ", "Java SE 6" };

        File fbyte = new File("D:\\ExampleFile.txt");

        File fsymb = new File("D:\\FileTextExample.txt");

        try {

            FileOutputStream fos =

                    new FileOutputStream(fbyte);

            FileWriter fw = new FileWriter(fsymb);

            for (String a : pArray) {

                fos.write(a.getBytes());

                fw.write(a);

            }

            fos.close();

            fw.close();

        } catch (IOException e) {

            System.err.println("ошибка файла: " + e);

        }

    }

}