package ProcessExample;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by Виктория on 11.11.2016.
 */
public class Task2 {
    public static void main(String[] args) throws IOException, InterruptedException {
        Process process=new ProcessBuilder().command("cmd","dir").start();
        BufferedReader reader=new BufferedReader(new InputStreamReader(process.getInputStream()));
        String line;
        while((line=reader.readLine())!=null){
            System.out.println(line);
        }
        reader.close();
        process.waitFor();
    }
}


