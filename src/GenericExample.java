/**
 * Created by Виктория on 06.11.2016.
 */
public class GenericExample<T> {
        private T t;

        public T get(){
            return this.t;
        }

        public void set(T t1){
            this.t=t1;
        }

        public static void main(String args[]){
            GenericExample<String> type = new GenericExample<>();
            type.set("Pankaj"); //valid

            GenericExample type1 = new GenericExample(); //raw type
            type1.set("Pankaj"); //valid
            type1.set(10); //valid and autoboxing support
        }
    }